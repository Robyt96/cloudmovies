# CloudMovies

CloudMovies - A simple application to search movies info and posters, based on a microservices architecture.

## The application

In this section are described the basic informations about each microservices of the application and the prerequisites and the instruction to run it.

### Architecture
![](cloudmovies.png)

* Ambassador is the API gateway: it receives and forwards all the http requests directed to the backend microservices
* Authentication service is responsible for validate the requests before the gateway forwards them to a protected microservice
* Movies service exposes API useful to get movies information or search movies by title or genre
* Userlists service exposes CRUD API for managing personal movies' lists
* Faas getPoster exposes an API useful to obtain the URL of the poster of a movie
* Faas getTrailer exposes an API useful to obtain the URL of the trailer of a movie
* WebApp is the service which run the webserver for the frontend website

### Prerequisites

This application requires a running Kubernetes cluster wiht Openfaas installed as described [here](https://docs.openfaas.com/deployment/kubernetes/).
This application was developed and tested on different machines:
 * Debian 10.2 Azure Lab Virtual Machine with Kubernetes v1.16.2, minikube v1.5.2 and Docker 19.03.4
 * CentOS 7 Azure B2s Standard Machine with Kubernetes v1.17, minikube v1.8.2 and Docker 19.03.8

### Installing

These steps have to be followed in order to launch and use the application

* Clone this repository on your machine
* Create the folders /data/userlistsdb, /data/userdb, /data/moviedb for persistent storage of DBs
* Create the file tmdb_api_key.txt containing your TMDB apikey and move it in the root project folder
* From root project folder execute the file setup.sh
* Wait a few seconds for all pods status to become ready. Then execute the following port-forward commands:
  * `kubectl port-forward svc/ambassador 8088 --address 0.0.0.0`
  * `kubectl port-forward -n cloudmovies svc/websvc 8080:80 --address 0.0.0.0`
* Now the application should be deployed: at http://*your-machine-ip-address*:8080 is available the web application; at http://*your-machine-ip-address*:8088 you can make http requests directly to the API gateway

### Testing

To make sure that everything works, you can easily launch the automatic unit tests going in tests folder and run the command:
```
docker run --network="host" --name="testsc" --rm -it --mount type=bind,source="$(pwd)",target=/app nyurik/alpine-python3-requests python /app/tests.py
```

## Built With

* [Kubernetes](https://kubernetes.io/)
* [Docker](https://www.docker.com/)
* [Flask](https://www.palletsprojects.com/p/flask/)
* [MySQL](https://www.mysql.com/)
* [NGINX](https://www.nginx.com/)

## Authors
* Roberto Tagliabue 807409
* Andrea Vegetti 794298
* Haixing Chen 793027
* Andrea Tiralongo 794924

## License

This project is licensed under the GNU General Public License v3.0 License - see the [LICENSE.md](LICENSE.md) file for details