create table favorites (
  userid int,
  movieid int,
  created datetime,
  primary key (userid,movieid)
);

create table towatch (
  userid int,
  movieid int,
  created datetime,
  primary key (userid,movieid)
);
