from flask import Flask, request, make_response
from flask_restful import Resource, Api
import mysql.connector
from flask import jsonify
from flask_cors import CORS


app = Flask(__name__)
api = Api(app)
CORS(app)


cnx = None
def get_cnx():
    global cnx
    if cnx and cnx.is_connected():
        return cnx
    else:
        cnx = mysql.connector.connect(
            host='userlistsdbsvc',
            user='userlistsdb_user',
            passwd='userlistsdb_pwd',
            database='userlistsdb'
        )
        return cnx

def open_cnx():
    conn = mysql.connector.connect(
            host='userlistsdbsvc',
            user='userlistsdb_user',
            passwd='userlistsdb_pwd',
            database='userlistsdb'
        )
    return conn

def check_header(request):
    userid = request.headers.get('Cloudmovie-UserId','')
    if (not userid or not userid.isnumeric()):
        return False
    return True



class addFavorite(Resource):
    def get(self):
        # get user id
        if check_header(request):
            userid = int(request.headers['Cloudmovie-UserId'])
        else:
            responseObject = {
                'status': 'fail',
                'message': 'internal error: user id not found'
            }
            return make_response(jsonify(responseObject), 500)
        
        # get movieid parameter
        try:
            movieid = int(request.args['movieid'])
        except:
            responseObject = {
                'status': 'fail',
                'message': 'parameter incorrect'
            }
            return make_response(jsonify(responseObject), 500)
        
        # get number of favorites
        cur = get_cnx().cursor()
        query = "select count(*) from favorites where userid = %d" %userid
        cur.execute(query)
        num_of_favorites = cur.fetchone()[0]
        
        # insert record
        if (num_of_favorites < 5):
            query = (
                "insert into favorites (userid, movieid, created)"
                "values (%s, %s, now())" %(userid, movieid)
            )
            try:
                cur.execute(query)
                get_cnx().commit()
            except mysql.connector.Error as error:
                responseObject = {
                    'status': 'fail',
                    'message': 'movie already added'
                }
                return make_response(jsonify(responseObject), 500)
            # if insert ok
            responseObject = {
                'status': 'success',
                'message': 'movie added'
            }
            return make_response(jsonify(responseObject), 200)
        else:
            responseObject = {
                'status': 'fail',
                'message': 'max number of favorites reached'
            }
            return make_response(jsonify(responseObject), 500)
#

class deleteFavorite(Resource):
    def get(self):
        # get user id
        if check_header(request):
            userid = int(request.headers['Cloudmovie-UserId'])
        else:
            responseObject = {
                'status': 'fail',
                'message': 'internal error: user id not found'
            }
            return make_response(jsonify(responseObject), 500)
        
         # get movieid parameter
        try:
            movieid = int(request.args['movieid'])
        except:
            responseObject = {
                'status': 'fail',
                'message': 'parameter incorrect'
            }
            return make_response(jsonify(responseObject), 500)
        
        # delete row
        cur = get_cnx().cursor()
        query = (
            "delete from favorites where userid = %s and movieid = %s"
            %(userid, movieid)
        )
        cur.execute(query)
        get_cnx().commit()
        responseObject = {
            'status': 'success',
            'message': str(cur.rowcount) + ' record(s) deleted'
        }
        return make_response(jsonify(responseObject), 200)
#

class getFavorites(Resource):
    def get(self):
        # get user id
        if check_header(request):
            userid = int(request.headers['Cloudmovie-UserId'])
        else:
            responseObject = {
                'status': 'fail',
                'message': 'internal error: user id not found'
            }
            return make_response(jsonify(responseObject), 500)
        
        # query
        result = ""
        conn = None
        cur = None
        try:
            query = "select movieid from favorites where userid = %s" %userid
            conn = open_cnx() 
            cur = conn.cursor()
            cur.execute(query)
            records = cur.fetchall()
            list_movieid = [r[0] for r in records]
            result = jsonify(list_movieid)
        except:
            #err = sys.exc_info()[0]
            #result = str(err)
            result = "DB error"
        finally:
            cur.close()
            conn.close()
            return result
#

class deleteAll(Resource):
    def delete(self):
        # get user id
        if check_header(request):
            userid = int(request.headers['Cloudmovie-UserId'])
        else:
            responseObject = {
                'status': 'fail',
                'message': 'internal error: user id not found'
            }
            return make_response(jsonify(responseObject), 500)
        
        # query
        cur = get_cnx().cursor()
        numRecordsDeleted = 0

        query1 = "delete from favorites where userid = %s" %userid
        cur.execute(query1)
        numRecordsDeleted += cur.rowcount

        query2 = "delete from towatch where userid = %s" %userid
        cur.execute(query2)
        numRecordsDeleted += cur.rowcount
        
        responseObject = {
            'status': 'success',
            'message': str(numRecordsDeleted) + ' record(s) deleted'
        }
        return make_response(jsonify(responseObject), 200)
#


class addToWatch(Resource):
    def get(self):
        # get user id
        if check_header(request):
            userid = int(request.headers['Cloudmovie-UserId'])
        else:
            responseObject = {
                'status': 'fail',
                'message': 'internal error: user id not found'
            }
            return make_response(jsonify(responseObject), 500)
        
        # get movieid parameter
        try:
            movieid = int(request.args['movieid'])
        except:
            responseObject = {
                'status': 'fail',
                'message': 'parameter incorrect'
            }
            return make_response(jsonify(responseObject), 500)
        
        # get number of towatch
        cur = get_cnx().cursor()
        query = "select count(*) from towatch where userid = %d" %userid
        cur.execute(query)
        num_of_towatch = cur.fetchone()[0]
        
        # insert record

        query = (
            "insert into towatch (userid, movieid, created)"
            "values (%s, %s, now())" %(userid, movieid)
        )
        try:
            cur.execute(query)
            get_cnx().commit()
        except mysql.connector.Error as error:
            responseObject = {
                'status': 'fail',
                'message': 'movie already added'
            }
            return make_response(jsonify(responseObject), 500)
        # if insert ok
        responseObject = {
            'status': 'success',
            'message': 'movie added'
        }
        return make_response(jsonify(responseObject), 200)
#

class deleteToWatch(Resource):
    def get(self):
        # get user id
        if check_header(request):
            userid = int(request.headers['Cloudmovie-UserId'])
        else:
            responseObject = {
                'status': 'fail',
                'message': 'internal error: user id not found'
            }
            return make_response(jsonify(responseObject), 500)
        
         # get movieid parameter
        try:
            movieid = int(request.args['movieid'])
        except:
            responseObject = {
                'status': 'fail',
                'message': 'parameter incorrect'
            }
            return make_response(jsonify(responseObject), 500)
        
        # delete row
        cur = get_cnx().cursor()
        query = (
            "delete from towatch where userid = %s and movieid = %s"
            %(userid, movieid)
        )
        cur.execute(query)
        get_cnx().commit()
        responseObject = {
            'status': 'success',
            'message': str(cur.rowcount) + ' record(s) deleted'
        }
        return make_response(jsonify(responseObject), 200)
#

class getToWatch(Resource):
    def get(self):
        # get user id
        if check_header(request):
            userid = int(request.headers['Cloudmovie-UserId'])
        else:
            responseObject = {
                'status': 'fail',
                'message': 'internal error: user id not found'
            }
            return make_response(jsonify(responseObject), 500)
        
        # query
        result = ""
        try:
            query = "select movieid from towatch where userid = %s" %userid
            conn = open_cnx() 
            cur = conn.cursor()
            cur.execute(query)
            records = cur.fetchall()
            list_movieid = [r[0] for r in records]
            result = jsonify(list_movieid)
        except:
            result = "DB error"
        finally:
            cur.close()
            conn.close()
            return result
#

api.add_resource(addFavorite, '/addfavorite')
api.add_resource(deleteFavorite, '/deletefavorite')
api.add_resource(getFavorites, '/getfavorites')

api.add_resource(addToWatch, '/addtowatch')
api.add_resource(deleteToWatch, '/deletetowatch')
api.add_resource(getToWatch, '/gettowatch')
api.add_resource(deleteAll, '/deleteall')
