//var api_url = "http://localhost:8088";
var api_url = "http://" + window.location.hostname + ":8088";
console.log(api_url);
var token = "";
var current_user = "";

// login handling
function register() {
    var username = $('#username').val();
    var password = $('#password').val();

    if (username.length < 4) {
        alert("Username too short (min 4 char)");
        return false;
    }
    if (password.length < 6) {
        alert("Password too short (min 6 char)");
        return false;
    }

    var url = api_url + "/auth/register"
    data = "user=" + encodeURI(username) + "&pwd=" + encodeURI(password);
    //console.log(data)
    $.ajax({
        type: "POST",
        url: url,
        data: data,
        statusCode: {
            200: function(result) {
                $('#username').val('');
                $('#password').val('');
                openSession(result.auth_token, username);
            },
            202: function(result) {
                $("#loginError").html('User already exists');
                setTimeout(function() { $("#loginError").html(''); }, 3000);
            },
            500: function(result) {
                $("#loginError").html('Server error');
                setTimeout(function() { $("#loginError").html(''); }, 3000);
            }
        }
    });
}

function login() {
    var username = $('#username').val();
    var password = $('#password').val();

    var url = api_url + "/auth/login"
    data = "user=" + encodeURI(username) + "&pwd=" + encodeURI(password);
    //console.log(data)
    $.ajax({
        type: "POST",
        url: url,
        data: data,
        success: function(result){
            //$("#mainContentDiv").html(JSON.stringify(result));
            
            
            // svuoto campi form username e password
            $('#username').val('');
            $('#password').val('');
            openSession(result.auth_token, username);
        },
        error: function(msg) {
            $("#loginError").html('Username or password are incorrect');
            setTimeout(function() { $("#loginError").html(''); }, 3000);
        }
    });
}

function deleteUser() {
    if (!token) return;

    if (confirm('Are you sure? All your lists will be lost')) {
        var url = api_url + "/auth/deleteuser";    
        $.ajax({
            type: "DELETE",
            url: url,
            beforeSend: function(request) {
                request.setRequestHeader("Authorization", "Bearer " + token);
            },
            success: function(result){
                closeSession();
            },
            error: function(msg) {
                console.log('error: ' + JSON.stringify(msg));
            }
        });
    }
}

function openSession(t, u) {
	token = t;
	current_user = u;
    sessionStorage.setItem('token', token);
    sessionStorage.setItem('current_user', current_user);
    $('#favorites').show('slow');
    $('#towatch').show('slow');
	$('#loginDiv').hide('slow');
	$('#welcomeDiv').show('slow');
    $('#welcomemsg').html('Welcome <i>'+ current_user + '</i>');
	getAllLists();
}

function logout() {
	alert("Successfully logged out!");
	closeSession();
}

function closeSession() {
	token = "";
    current_user = "";
    sessionStorage.removeItem('token');
    sessionStorage.removeItem('current_user');
    favoritesList = new Array();
    towatchList = new Array();
    $('#favorites').hide('slow');
    $('#towatch').hide('slow');
	$('#loginDiv').show('slow');
	$('#welcomeDiv').hide('slow');
    getMovieList();
}

function checkSession( ) {
    if (sessionStorage.getItem('token')) {
        openSession(sessionStorage.getItem('token'), sessionStorage.getItem('current_user'));
    }
}

// movie db handling
var maxResults = 20; //number of movies expected from the backend

function getMovieList(start) {
    var url = api_url + "/getlist";
    orderby=$('input[name=orderby]:checked').val();
    sorting=$('input[name=sorting]:checked').val();
    
    if(start) 
    {
        
        url += "?start=" + start;
        if(orderby=="vote")
        {
            if(sorting=="asc")
            {
                url += "&sorting=" + sorting;
            }
        }

        if(orderby=="release_date")
        {
            url += "&orderby=" + orderby;
            if(sorting=="asc")
            {
                url += "&sorting=" + sorting;
            }
        }

    }
    
    else
    {
        if(orderby=="vote")
        {
            if(sorting=="asc")
            {
                url += "?sorting=" + sorting;
            }
        }

        if(orderby=="release_date")
        {
            url += "?orderby=" + orderby;
            if(sorting=="asc")
            {
                url += "&sorting=" + sorting;
            }
        }

    }

    var myTemplate = $.templates("#mainDivTmpl");
    $.ajax({
        type: "GET",
        url: url,
        success: function(result){
        	//$("#mainTitleDiv").html("<b>Best Movies</b>");
        	if (!start) {
        		$("#mainContentDiv").empty();
        		result.title = "Movies";
        	}
        	//se result.movies ha dim maxResults: result.showmore = true; e result.start = start ? start+maxResults : maxResults;
        	if (result.movies.length === maxResults) {
                result.showmore = true;
                result.start = start ? start+maxResults : maxResults;
                result.getmovielist = true;
        	}
        	if (start) $('#showMoreButton'+start).hide();
        	//il pulsante showmore chiama getMovieList(result.start)
        	//la funzione deve fare append in maincontentdiv se e solo se start e' valorizzato
        	//se result.movies ha dim < maxResults nascondo pulsante (nel senso di display none)

        	//idea, da capire: metto pulsante nel template con id "showMoreButton"+result.start
        	//quando chiamo la getMovieList con start valorizzato nascondo il pulsante corrispondente
            $("#basicSearchDiv").show();
            $("#orderButtons").show();
            $("#advancedSearchDiv").hide();
            $("#mainContentDiv").append(myTemplate.render(result));
        },
        error: function(msg) {
            console.log(JSON.stringify(msg));
        }
    })
}

function infoMovie(id) {
    //$("#mainContentDiv").html(id);
    //console.log('infoMovie called');
    var url = api_url + "/movie/" + id;
    //console.log(id)
    var myTemplate = $.templates("#movieInfoTmpl");
    var result = null;
    $.ajax({
        type: "GET",
        url: url,
        success: function(resp) {
            result = resp
            if (token) {
                if (favoritesList.includes(result.info.id)) {
                    result.info.isfavorite = "delete";
                }
                else {
                    result.info.isfavorite = "add";
                }
                if (towatchList.includes(result.info.id)) {
                    result.info.istowatch = "delete";
                }
                else {
                    result.info.istowatch = "add";
                }
            }
            
            $.ajax({
                type: "GET",
                url: api_url + "/function/gettrailer?movieid=" + id,
                success: function(resp) {
                    result.info.youtube = resp.trailerurl.replace("watch?v=", "embed/");
                    $("#basicSearchDiv").hide();
                    $("#advancedSearchDiv").hide();
                    $("#orderButtons").hide();
                    $("#mainContentDiv").html(myTemplate.render(result.info));
                },
                statusCode: {
                    404: function() {
                        $("#basicSearchDiv").hide();
                        $("#advancedSearchDiv").hide();
                        $("#orderButtons").hide();
                        $("#mainContentDiv").html(myTemplate.render(result.info));
                    }
                }
            });
          
        },
        error: function(msg) {
            $("#mainContentDiv").html('error' + JSON.stringify(msg));
        }
    });
}


function searchMovie(url, start) {
    orderby=$('input[name=orderby]:checked').val();
    sorting=$('input[name=sorting]:checked').val();
    var newUrl = url;
    if (start) newUrl += "&start=" + start;
    var myTemplate = $.templates("#mainDivTmpl");
    newUrl += "&orderby=" + orderby;
    newUrl += "&sorting=" + sorting;
    $.ajax({
        type: "GET",
        url: newUrl,
        success: function(result){
            // set title only if start is not passed
            if (!start) {
                $("#mainContentDiv").empty();
                result.title = "Search results";
            }
            
            // set fields for the showmore button if there are enough results
            if (result.movies.length === maxResults) {
                result.showmore = true;
                result.start = start ? start+maxResults : maxResults;
                result.search = true;
                result.url = url;
            }

            // hide previous button
            if (start) $('#showMoreButton'+start).hide();
            
            // append result or show warning
            if (result.movies.length !== 0) {
                $("#mainContentDiv").append(myTemplate.render(result));
            } else {
                if (!start) {
                    $("#mainContentDiv").html("<div id='mainTitleDiv'>No results found!</div>");
                } else {
                    $("#mainContentDiv").append("<div id='mainTitleDiv'>No results found!</div>");
                }
            }
        },
        error: function() {
            alert('Some error occured. Please try again');
        }
    });
}

function getMoviesByIds(idlist, list) {
	// idlist deve essere un array javascript di id, es. quello dei favoriti
	//chiamo al solito il backend con questo elenco e mostro risultati
	var url = api_url + "/search?id=" + idlist.join();
    var myTemplate = $.templates("#mainDivTmpl");
    var title = list === 'favorites' ? "My favorites" : "To Watch"
    $.ajax({
        type: "GET",
        url: url,
        success: function(result){
           // $("#mainContentDiv").html(JSON.stringify(result));
            result.title = title;
            $("#mainContentDiv").html(myTemplate.render(result));
        },
        error: function() {
            $("#mainContentDiv").html('error');
        }
    });
}

function getPoster(movieId) {
    var url = api_url + "/function/getposters?movieid=" + movieId;
    //console.log(url);
    $.ajax({
        type: "GET",
        url: url,
        success: function(result){
            document.getElementById("poster"+movieId).src = result.url;
        },
        error: function(msg) {
            //$("#mainContentDiv").html('error');
            console.log(msg);
        }
    });
}


//favorites handling
var favoritesList = new Array( );
var towatchList = new Array( );

function getAllLists() {
	//called at login and at every list toggle

	if (!token) return;

	var url = api_url + "/getfavorites";    
    $.ajax({
        type: "GET",
        url: url,
        beforeSend: function(request) {
			request.setRequestHeader("Authorization", "Bearer " + token);
		},
        success: function(result){
        	// valorizzo myFavorites (array)
        	favoritesList = result;
        	console.log(JSON.stringify(result));
        },
        error: function(msg) {
            console.log('error: ' + JSON.stringify(msg));
        }
    });

    var url2 = api_url + "/gettowatch"; 
    $.ajax({
        type: "GET",
        url: url2,
        beforeSend: function(request) {
			request.setRequestHeader("Authorization", "Bearer " + token);
		},
        success: function(result){
        	// valorizzo myFavorites (array)
        	towatchList = result;
        	console.log(JSON.stringify(result));
        },
        error: function(msg) {
            console.log('error: ' + JSON.stringify(msg));
        }
    });

}

function showList(list) {
    if (!token) return;
    var listToShow;
    if (list === 'favorites') {
        listToShow = favoritesList;
    }
    else if (list === 'towatch') {
        listToShow = towatchList;
    }
    else {
        console.log("Error " + list);
    }
    $("#basicSearchDiv").hide();
    $("#advancedSearchDiv").hide();
    $("#orderButtons").hide();

	if (typeof listToShow !== 'undefined' && listToShow.length > 0) {
    // the array is defined and has at least one element
        getMoviesByIds(listToShow, list);
    } else {
    	$("#mainContentDiv").html("<div id='mainTitleDiv'>Your list is empty!</div>");
    }
}

function toggleList(movieid, listToToggle) {
    if (!token) return;

    //if non e' preferito:
    if (listToToggle === 'favorites') {
        if (!favoritesList.includes(movieid)) {
            if (favoritesList.length > 5) {
                alert("You have already chosen 5 favorites!");
                return;
            }
            else {
                var url = api_url + "/addfavorite?movieid=" + movieid;    
                $.ajax({
                    type: "GET",
                    url: url,
                    beforeSend: function(request) {
                        request.setRequestHeader("Authorization", "Bearer " + token);
                    },
                    success: function(result){
                        favoritesList.push(movieid);
                        $("#favoriteIcon").attr("src", "images/deletefavorite.png");
                    },
                    error: function(msg) {
                        console.log('error: ' + msg);
                    }
                });
            }
        }
        else {
            var url = api_url + "/deletefavorite?movieid=" + movieid;    
            $.ajax({
                type: "GET",
                url: url,
                beforeSend: function(request) {
                    request.setRequestHeader("Authorization", "Bearer " + token);
                },
                success: function(result){
                    favoritesList = $.grep(favoritesList, function(value) { return value != movieid; });
                    $("#favoriteIcon").attr("src", "images/addfavorite.png");
                },
                error: function(msg) {
                    console.log('error: ' + msg);
                }
            });
        }
    }
    else if (listToToggle === 'towatch') {
        if (!towatchList.includes(movieid)) {
           var url = api_url + "/addtowatch?movieid=" + movieid;    
            $.ajax({
                type: "GET",
                url: url,
                beforeSend: function(request) {
                    request.setRequestHeader("Authorization", "Bearer " + token);
                },
                success: function(result){
                    towatchList.push(movieid);
                    $("#towatchIcon").attr("src", "images/deletetowatch.png");
                },
                error: function(msg) {
                    console.log('error: ' + msg);
                }
            });
        }
        else {
            var url = api_url + "/deletetowatch?movieid=" + movieid;    
            $.ajax({
                type: "GET",
                url: url,
                beforeSend: function(request) {
                    request.setRequestHeader("Authorization", "Bearer " + token);
                },
                success: function(result){
                    towatchList = $.grep(towatchList, function(value) { return value != movieid; });
                    $("#towatchIcon").attr("src", "images/addtowatch.png");
                },
                error: function(msg) {
                    console.log('error: ' + msg);
                }
            });
        }
    }

}

// called by onclick of "button" advancedSearch
function showAdvancedSearch() {
    $("#basicSearchDiv").hide();
    $("#advancedSearchDiv").load('./advancedSearch.html');
    $("#advancedSearchDiv").show();
    $("#searchMovieInput").val('');
}

function showBasicSearch() {
    $("#basicSearchDiv").show();
    $("#advancedSearchDiv").hide();
    $("#filterTitle").val('');
    $("#filterGenres").val('');
    
}

// submit genres form handler
function onBasicSearchSubmit() {
    var title = $('#searchMovieInput').val();
    var url = api_url + "/search?title=" + encodeURI(title);

    searchMovie(url);
}

function onAdvSearchSubmit() {
    var params = "";

    var title = $("#filterTitle").val();
    if (title) params += (params ? "&" : "?") + "title=" + encodeURI(title);

    var genres = $("#filterGenres option:selected").map(function () {
        return $(this).val();
    }).get().join(',');
    if (genres) params += (params ? "&" : "?") + "genres=" + encodeURI(genres);

    if (!params) {
        alert("Choose at least a filter");
        return false;
    }

    var url = api_url + "/search" + params;
    searchMovie(url);
}

// order and sorting radio buttons

function buttonEvent() {
    var title = $("#filterTitle").val();
    var genres = $("#filterGenres option:selected").map(function () {return $(this).val();}).get().join(',');
    if (title || genres){
        onAdvSearchSubmit();
    }
    else{
        if ($('#searchMovieInput').val()){
            onBasicSearchSubmit();
        }
        else{
            getMovieList();
        }
    }
}

function hidemunu(icon) {
    icon.classList.toggle("change");
  var x = document.getElementById("menuCol");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}
;
